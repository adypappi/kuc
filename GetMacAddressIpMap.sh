#!/usr/bin/env bash
#Get map MacAddress@IpAddress  using `ip addr`  command
# Display the result as "<mac1=ip1 mac2=ip2 ..... macn=ipn"
# Get name of all nics
declare -a allNicNames=($(ip addr | grep -Po "^\d+:\s\w+:\s" |cut -d':' -f2|xargs))
export lg=${#allNicNames[@]}

# Get the Mac address and the ip associated to it if exists.
res=""
for (( i=0; i<$lg; i++ ))
do
  device=$(ip a show ${allNicNames[$i]}) 
  mac=$(echo $device | grep -Po 'link/.*((\w\w:){5}\w\w)' |cut -d' ' -f2)
  ip=$(echo $device | grep -Po '(?<=inet )((\d+\.){3}\d+/?\d+)')
  res="$res\n$mac=$ip"
done
echo -e ${res:2}

