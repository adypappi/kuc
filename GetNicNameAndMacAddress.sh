#!/usr/bin/env bash
#Get and return nicName1:nicName1MacAddress;nicName2:nicName2MacAddress;......;nicNameM:nicNameMMacAddress 
#The script uses the pseudo file /sys/class/net
dataSource=/sys/class/net
res=""
for nics in $(ls $dataSource)
  do
   res=$res"$nics:$(cat $dataSource/$nics/address);"
done

printf "$res\n"
